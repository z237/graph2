#include <map>
#include <iostream>
#include <utility>
#include <cstdio>
#include <vector>
#include <string>
#include <fstream>
#include <sstream>
#include <set>

using namespace std;

class DSU{
public:
	int N;
	vector<int> p;
	DSU(int _N): N(_N) {
		p.resize(_N);
		for (int i = 0; i < p.size(); i++) {
			p[i] = i;
		}
	}

	int find(int v) {
		return (v == p[v]) ? v : (p[v] = this->find(p[v]));
	}

	void unite(int a, int b) {
		a = this->find(a);
		b = this->find(b);
		if (rand() & 1) {
			swap(a, b);
		}
		if (a != b) {
			p[a] = b;
		}
	}
};

class Graph {

public:
	Graph() {
		W = R = false;
		M = 0; N = 0; type = 'E';
	}

	Graph(int _N) : N(_N) {
		W = R = false;
		M = 0; type = 'E';
	}

	void readGraph(string filename) {
		ifstream in(filename);
		in >> type;
		if (type == 'C') {
			in >> N >> R >> W;
			adjMatrix.resize(N, vector<int>(N, 0));

			int x;
			for (int i = 0; i < N; i++) {
				for (int j = 0; j < N; j++) {
					in >> x;
					adjMatrix[i][j] = (W == 1) ? x : (x > 0) ? 1 : 0;
				}
			}
		}

		if (type == 'L') {
			in >> N >> R >> W;
			string buffer;
			getline(in, buffer);

			if (!W) {
				adjList.resize(N);
				for (int i = 0; i < N; i++) {
					getline(in, buffer);
					stringstream parser(buffer);
					int x;
					if (buffer == "") {
						continue;
					}
					while (!parser.eof() && buffer != "") {
						parser >> x;
						adjList[i].insert(x);
					}
				}
			}
			else {
				wAdjList.resize(N);
				for (int i = 0; i < N; i++) {
					getline(in, buffer);
					stringstream parser(buffer);
					int x, w;
					if (buffer == "") {
						continue;
					}
					while (!parser.eof()) {
						parser >> x >> w;
						wAdjList[i][x] = w;
					}
				}
			}
		}
		if (type == 'E') {
			in >> N >> M >> R >> W;
			if (!W) {
				int x, y;
				for (int i = 0; i < M; i++) {
					in >> x >> y;
					listOfEdges.insert(make_pair(x, y));
				}
			}
			else {
				int x, y, w;
				for (int i = 0; i < M; i++) {
					in >> x >> y >> w;
					wListOfEdges[make_pair(x, y)] = w;
				}
			}
		}
	}

	void addEdge(int from, int to, int weight) {
		M++;
		if (type == 'C') {
			from--; to--;
			adjMatrix[from][to] = weight;
			if (!R) {
				adjMatrix[to][from] = weight;
			}
		}
		if (type == 'L') {
			from--;
			if (!W) {
				adjList[from].insert(to);
				if (!R) {
					adjList[to].insert(from);
				}
			}
			else {
				wAdjList[from][to] = weight;
				if (!R) {
					wAdjList[to][from] = weight;
				}
			}
		}
		if (type == 'E') {
			if (!W) {
				listOfEdges.insert(make_pair(from, to));
			}
			else {
				wListOfEdges[make_pair(from, to)] = weight;
			}
		}
	}

	void removeEdge(int from, int to) {
		M--;
		if (type == 'C') {
			from--; to--;
			adjMatrix[from][to] = 0;
			M--;
			if (!R) {
				adjMatrix[to][from] = 0;
			}
		}
		if (type == 'L') {
			if (!W) {
				adjList[from - 1].erase(to);
				if (!R) {
					adjList[to - 1].erase(from);
				}
			}
			else {
				wAdjList[from - 1].erase(to);
				if (!R) {
					wAdjList[to - 1].erase(from);
				}
			}
		}
		if (type == 'E') {
			if (!W) {
				listOfEdges.erase(make_pair(from, to));
				if (!R) {
					listOfEdges.erase(make_pair(to, from));
				}
			}
			else {
				wListOfEdges.erase(make_pair(from, to));
				if (!R) {
					wListOfEdges.erase(make_pair(to, from));
				}
			}
		}
	}

	int changeEdge(int from, int to, int newWeight) {
		int prevWeight;
		if (type == 'C') {
			from--; to--;
			prevWeight = adjMatrix[from][to];
			adjMatrix[from][to] = newWeight;
			if (!R) {
				adjMatrix[to][from] = newWeight;
			}
		}
		if (type == 'L') {
			if (!W) {
				prevWeight = 0;
			}
			else {
				prevWeight = wAdjList[from - 1][to];
				wAdjList[from - 1][to] = newWeight;
				if (!R) {
					wAdjList[to - 1][from] = newWeight;
				}
			}
		}
		if (type == 'E') {
			if (!W) {
				prevWeight = 0;
			}
			else {
				if (R) {
					prevWeight = wListOfEdges[make_pair(from, to)];
					wListOfEdges[make_pair(from, to)] = newWeight;
				}
				else {
					if (wListOfEdges.find(make_pair(from, to)) != wListOfEdges.end()) {
						prevWeight = wListOfEdges[make_pair(from, to)];
						wListOfEdges[make_pair(from, to)] = newWeight;
					}
					else {
						prevWeight = wListOfEdges[make_pair(to, from)];
						wListOfEdges[make_pair(to, from)] = newWeight;
					}
				}
			}
		}
		return prevWeight;
	}

	void transformToAdjList() {
		if (type == 'L') {
			return;
		}
		if (!W) {
			adjList.resize(N);
		}
		else {
			wAdjList.resize(N);
		}

		if (type == 'C') {
			for (int i = 0; i < N; i++) {
				for (int j = 0; j < N; j++) {
					if (adjMatrix[i][j] != 0) {
						if (!W) {
							adjList[i].insert(j + 1);
						}
						else {
							wAdjList[i].insert(make_pair(j + 1, adjMatrix[i][j]));
						}
					}
				}
			}
			adjMatrix.clear();
		}
		if (type == 'E') {
			if (!W) {
				for (auto x : listOfEdges) {
					adjList[x.first - 1].insert(x.second);
					if (!R) {
						adjList[x.second - 1].insert(x.first);
					}
				}
			}
			else {
				for (auto x : wListOfEdges) {
					wAdjList[x.first.first - 1].insert(make_pair(x.first.second, x.second));
					if (!R) {
						wAdjList[x.first.second - 1].insert(make_pair(x.first.first, x.second));
					}
				}
			}
			listOfEdges.clear();
			wListOfEdges.clear();
		}
		type = 'L';
	}

	void transformToAdjMatrix() {
		if (type == 'C') {
			return;
		}
		adjMatrix.resize(N, vector<int>(N, 0));
		if (type == 'L') {
			if (!W) {
				for (int i = 0; i < N; i++) {
					for (int x : adjList[i]) {
						adjMatrix[i][x - 1] = 1;
					}
				}
				adjList.clear();
			}
			else {
				for (int i = 0; i < N; i++) {
					for (auto x : wAdjList[i]) {
						adjMatrix[i][x.first - 1] = x.second;
					}
				}
				wAdjList.clear();
			}
		}
		if (type == 'E') {
			if (!W) {
				for (pair<int, int> x : listOfEdges) {
					adjMatrix[x.first - 1][x.second - 1] = 1;
					if (!R) {
						adjMatrix[x.second - 1][x.first - 1] = 1;
					}
				}
				listOfEdges.clear();
			}
			else {
				for (auto x : wListOfEdges) {
					adjMatrix[x.first.first - 1][x.first.second - 1] = x.second;
					if (!R) {
						adjMatrix[x.first.second - 1][x.first.first - 1] = x.second;
					}
				}
				wListOfEdges.clear();
			}
		}
		type = 'C';
	}

	void transformToListOfEdges() {
		if (type == 'E') {
			return;
		}

		if (type == 'C') {
			if (R) {
				for (int i = 0; i < N; i++) {
					for (int j = 0; j < N; j++) {
						if (adjMatrix[i][j] != 0) {
							if (!W) {
								listOfEdges.insert(make_pair(i + 1, j + 1));
							}
							else {
								wListOfEdges.insert(make_pair(make_pair(i + 1, j + 1), adjMatrix[i][j]));
							}
						}
					}
				}
			}
			else {
				for (int i = 0; i < N; i++) {
					for (int j = i; j < N; j++) {
						if (adjMatrix[i][j] != 0) {
							if (!W) {
								listOfEdges.insert(make_pair(i + 1, j + 1));
							}
							else {
								wListOfEdges.insert(make_pair(make_pair(i + 1, j + 1), adjMatrix[i][j]));
							}
						}
					}
				}
			}
			adjMatrix.clear();
		}

		if (type == 'L') {
			if (!W) {
				for (int i = 0; i < N; i++) {
					for (int x : adjList[i]) {
						if (!R) {
							listOfEdges.erase(make_pair(x, i + 1));
						}
						listOfEdges.insert(make_pair(i + 1, x));
					}
				}
				adjList.clear();
			}
			else {
				for (int i = 0; i < N; i++) {
					for (auto x : wAdjList[i]) {
						if (!R) {
							wListOfEdges.erase(make_pair(x.first, i + 1));
						}
						wListOfEdges[make_pair(i + 1, x.first)] = x.second;
					}
				}
				wAdjList.clear();
			}
		}


		if (!W) {
			M = listOfEdges.size();
		}
		else {
			M = wListOfEdges.size();
		}

		type = 'E';
	}

	void writeGraph(string filename) {
		ofstream out(filename);
		if (type == 'C') {
			out << type << ' ' << N << endl << R << ' ' << W << endl;
			for (int i = 0; i < N; i++) {
				for (int j = 0; j < N; j++) {
					out << adjMatrix[i][j] << ' ';
				}
				out << endl;
			}
		}
		if (type == 'L') {
			out << type << ' ' << N << endl << R << ' ' << W << endl;
			if (!W) {
				for (int i = 0; i < N; i++) {
					for (int x : adjList[i]) {
						out << x << ' ';
					}
					out << endl;
				}
			}
			else {
				for (int i = 0; i < N; i++) {
					for (auto x : wAdjList[i]) {
						out << x.first << ' ' << x.second << ' ';
					}
					out << endl;
				}
			}
		}
		if (type == 'E') {
			out << type << ' ' << N << ' ' << M << endl << R << ' ' << W << endl;
			if (!W) {
				for (auto x : listOfEdges) {
					out << x.first << ' ' << x.second << endl;
				}
			}
			else {
				for (auto x : wListOfEdges) {
					out << x.first.first << ' ' << x.first.second << ' ' << x.second << endl;
				}
			}
		}

	}

	Graph getSpaingTreePrima() {
		Graph spaingTree(N);
		spaingTree.type = 'E';
		spaingTree.W = 1;
		Graph g = *this;
		g.transformToAdjMatrix();
		vector<bool> used(N, false);
		const int INF = 1e9;
		vector<int> min_e(N, INF), sel_e(N, -1);
		int weight = 0;
		min_e[0] = 0;
		for (int i = 0; i < N; i++) {
			int v = -1;
			for (int j = 0; j < N; j++) {
				if (!used[j] && (v == -1 || min_e[j] < min_e[v])) {
					v = j;
				}
			}
			if (min_e[v] == INF) {
				return Graph();
			}
			used[v] = true;
			if (sel_e[v] != -1) {
				spaingTree.addEdge(v + 1, sel_e[v] + 1, g.adjMatrix[v][sel_e[v]]);
				weight += g.adjMatrix[v][sel_e[v]];
			}

			for (int to = 0; to < N; to++) {
				if (g.adjMatrix[v][to] < min_e[to] && g.adjMatrix[v][to] != 0) {
					min_e[to] = g.adjMatrix[v][to];
					sel_e[to] = v;
				}
			}
		}
		//cout << weight << endl;
		return spaingTree;
	}

	Graph getSpaingTreeKruscal() {
		Graph spaingTree(N);
		spaingTree.W = 1;
		set<pair<int, pair<int, int> > > g;
		this->transformToListOfEdges();
		for (auto x : wListOfEdges) {
			g.insert(make_pair(x.second, x.first));
		}
		int weight = 0;
		DSU dsu(N);
		for (auto x : g) {
			int a = x.second.first;
			int b = x.second.second;
			int l = x.first;
			a--;
			b--;
			if (dsu.find(a) != dsu.find(b)) {
				weight += l;
				spaingTree.addEdge(a + 1, b + 1, l);
				dsu.unite(a, b);
			}
		}
		//cout << weight;
		return spaingTree;
	}

	Graph getSpaingTreeBoruvka() {
		Graph spaingTree(N);
		spaingTree.W = 1;
		set<pair<int, pair<int, int> > > g;
		this->transformToListOfEdges();
		for (auto x : wListOfEdges) {
			g.insert(make_pair(x.second, x.first));
		}
		int weight = 0;
		DSU dsu(N);
		const int INF = 1e9;
		vector<int> w(N, INF);
		set<int> comp;
		for (int i = 0; i < N; i++) {
			comp.insert(i);
		}
		set<pair<int, pair<int, int> > > store;
		while (comp.size() != 1) {
			vector < pair<int, pair<int, int> > > wx(N, make_pair(INF, make_pair(0, 0)));
			for (auto x : g) {
				int a = x.second.first;
				int b = x.second.second;
				int l = x.first;
				a--;
				b--;
				if (dsu.find(a) != dsu.find(b)) {
					int A = dsu.find(a);
					int B = dsu.find(b);
					if (wx[A].first > l) {
						wx[A] = x;
					}
					if (wx[B].first > l) {
						wx[B] = x;
					}
				}
			}
			set<int> tmp;
			for (int i = 0; i < N; i++) {
				int a = dsu.find(i);
				auto edge = wx[a];
				if (wx[a].first == INF) {
					continue;
				}
				dsu.unite(edge.second.first - 1, edge.second.second - 1);
				tmp.insert(dsu.find(i));
				store.insert(wx[a]);
			}
			swap(tmp, comp);
		}
		for (auto x : store) {
			spaingTree.addEdge(x.second.first, x.second.second, x.first);
			weight += x.first;
		}
		//cout << weight;
		return spaingTree;
	}
	bool W, R;
	int N, M;
	char type;
	vector<vector<int> > adjMatrix;
	vector<set<int> > adjList;
	vector<map<int, int> > wAdjList;
	set<pair<int, int> > listOfEdges;
	map<pair<int, int>, int> wListOfEdges;
};

int main() {
	Graph G;
	G.readGraph("input.txt");
	(G.getSpaingTreeBoruvka()).writeGraph("output.txt");
	return 0;
}